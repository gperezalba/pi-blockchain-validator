#!/bin/sh
mv ~/pi-blockchain-validator/parity/parity.log ~/pi-blockchain-validator/parity/parity_old.log
killall screen
screen -S parity-node -d -m 
screen -S parity-node -X stuff "cd pi-blockchain-validator/parity/\n"
screen -S parity-node -X stuff "./parity --config node.toml\n"