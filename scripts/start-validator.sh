#!/bin/sh
mv ~/UTC* ~/pi-blockchain-validator/parity/node/keys/Pi-Chain-3/
cp ~/pi-blockchain-validator/parity/node_original.toml ~/pi-blockchain-validator/parity/node.toml
sed -i "s/0xf6bd003d07eba2027c34face6af863fd3f8b5a14/"$1"/g" ~/pi-blockchain-validator/parity/node.toml
chmod u+x ~/pi-blockchain-validator/parity/parity
screen -S parity-node -d -m 
screen -S parity-node -X stuff "cd pi-blockchain-validator/parity/\n"
screen -S parity-node -X stuff "./parity --config node.toml\n"