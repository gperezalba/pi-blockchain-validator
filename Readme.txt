COPY WALLET
scp -i node.pem UTC--2019-06-03T15-47-04Z--cd214691-0fc1-34e6-f36a-1079cccc516a ubuntu@ec2-54-217-25-177.eu-west-1.compute.amazonaws.com:~/pi-blockchain-validator/parity/node/keys/Pi-Chain-3/UTC--2019-06-03T15-47-04Z--cd214691-0fc1-34e6-f36a-1079cccc516a

START VALIDATOR
ssh -i node.pem ubuntu@ec2-54-217-25-177.eu-west-1.compute.amazonaws.com 'sh -s' < start.sh "0xg6bd003d07eba2027c34face6af863fd3f8b5a14"

CHECK STATUS
ssh -i node.pem ubuntu@ec2-54-217-25-177.eu-west-1.compute.amazonaws.com 'sh pi-blockchain-validator/scripts/check-status.sh'
scp -i node.pem ubuntu@ec2-54-217-25-177.eu-west-1.compute.amazonaws.com:~/pi-blockchain-validator/parity/status.log ./status.log

RESTART VALIDATOR
ssh -i node.pem ubuntu@ec2-54-217-25-177.eu-west-1.compute.amazonaws.com 'sh pi-blockchain-validator/scripts/restart-validator.sh'
scp -i node.pem ubuntu@ec2-54-217-25-177.eu-west-1.compute.amazonaws.com:~/pi-blockchain-validator/parity/parity.log ./parity_old.log

STOP VALIDATOR
ssh -i node.pem ubuntu@ec2-54-217-25-177.eu-west-1.compute.amazonaws.com 'sh -s' < stop.sh